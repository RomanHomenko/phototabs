//
//  PhotoViewController.swift
//  photoTabs
//
//  Created by roman on 6/17/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {

    var image: UIImage?
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBAction func shareAction(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        photoImageView.image = image
    }
}
